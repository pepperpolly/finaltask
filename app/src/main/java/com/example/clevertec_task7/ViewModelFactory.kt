package com.example.clevertec_task7

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.clevertec_task7.ui.main.MainViewModel
import javax.inject.Inject
import javax.inject.Provider

class ViewModelFactory @Inject constructor(
    viewMainModelProvider: Provider<MainViewModel>
) :
    ViewModelProvider.Factory {
    private val providers =
        mapOf<Class<*>, Provider<out ViewModel>>(
            MainViewModel::class.java to viewMainModelProvider
        )

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return providers[modelClass]!!.get() as T
    }

}