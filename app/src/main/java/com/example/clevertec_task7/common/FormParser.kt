package com.example.clevertec_task7.common

import com.example.clevertec_task7.domain.model.FormItem
import com.example.clevertec_task7.domain.model.FullFormItem
import com.example.clevertec_task7.domain.model.Values
import org.json.JSONObject


class FormParser {
    fun getForm(response: String): FullFormItem {
        val formJson = JSONObject(response)
        val title = formJson.getString("title")
        val image = formJson.getString("image")
        val fields = formJson.optJSONArray("fields")
        val length = fields.length()
        var list = mutableListOf<FormItem>()
        for (i in 0 until length) {
            val field = fields.getJSONObject(i)
            if (field.getString("type") == "LIST") {
                val values = field.getJSONObject("values")
                val valuesString = Values(
                    values.getString("none"),
                    values.getString("v1"),
                    values.getString("v2"),
                    values.getString("v3")
                )
                list.add(
                    FormItem.ListItem(
                        field.getString("title"),
                        field.getString("name"),
                        field.getString("type"),
                        valuesString
                    )
                )
            } else {
                list.add(
                    FormItem.StringNumberItem(
                        field.getString("title"),
                        field.getString("name"),
                        field.getString("type"),
                    )
                )
            }
        }
        return FullFormItem(title, image, list)
    }

    fun getResponse(response: String): String {
        val responseString = JSONObject(response)
        return responseString.getString("result")
    }
}