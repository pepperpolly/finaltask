package com.example.clevertec_task7.common

interface StateListener {
    fun changeState(tile: String, state: String)
}