package com.example.clevertec_task7.data.remote

import com.example.clevertec_task7.domain.model.UserForm
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface FormApi {
    @GET("meta/")
    suspend fun getFormInfo(): String

    @POST("data/")
    suspend fun postForm(@Body form: UserForm): String
}