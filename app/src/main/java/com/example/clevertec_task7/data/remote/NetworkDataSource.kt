package com.example.clevertec_task7.data.remote

import com.example.clevertec_task7.domain.model.UserForm
import javax.inject.Inject

class NetworkDataSource @Inject constructor(private val formApi: FormApi) {

    suspend fun getFormInfo(): String = formApi.getFormInfo()

    suspend fun postForm(form: UserForm): String = formApi.postForm(form)

}