package com.example.clevertec_task7.data.repository

import com.example.clevertec_task7.domain.model.UserForm
import com.example.clevertec_task7.data.remote.NetworkDataSource
import javax.inject.Inject

class Repository @Inject constructor(private val networkDataSource: NetworkDataSource) {

    suspend fun getFormInfo(): String = networkDataSource.getFormInfo()

    suspend fun postForm(form: UserForm): String = networkDataSource.postForm(form)
}