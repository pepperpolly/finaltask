package com.example.clevertec_task7.di

import android.app.Application
import com.example.clevertec_task7.MainActivity
import com.example.clevertec_task7.ViewModelFactory
import com.example.clevertec_task7.ui.main.MainFragment
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Component(modules = [RepositoryModule::class, RetrofitModule::class, NetworkModule::class])
@Singleton
interface AppComponent {

    fun viewModelsFactory(): ViewModelFactory

    fun inject(mainActivity: MainActivity)

    fun inject(mainFragment: MainFragment)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}