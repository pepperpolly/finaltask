package com.example.clevertec_task7.di

import android.app.Application
import android.content.Context

class FormApplication : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder()
            .application(this)
            .build()
    }

}

val Context.appComponent: AppComponent
    get() = when (this) {
        is FormApplication -> this.appComponent
        else -> this.applicationContext.appComponent
    }