package com.example.clevertec_task7.di

import com.example.clevertec_task7.data.remote.FormApi
import com.example.clevertec_task7.data.remote.NetworkDataSource
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NetworkModule {
    @Provides
    @Singleton
    fun provideNetworkDataSource(
        formApi: FormApi
    ): NetworkDataSource {
        return NetworkDataSource(formApi)
    }
}