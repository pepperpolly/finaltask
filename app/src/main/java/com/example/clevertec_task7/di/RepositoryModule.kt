package com.example.clevertec_task7.di

import com.example.clevertec_task7.data.remote.NetworkDataSource
import com.example.clevertec_task7.data.repository.Repository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {
    @Provides
    @Singleton
    fun provideRepo(
        networkDataSource: NetworkDataSource
    ): Repository {
        return Repository(
            networkDataSource
        )
    }
}