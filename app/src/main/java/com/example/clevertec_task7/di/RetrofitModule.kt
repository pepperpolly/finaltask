package com.example.clevertec_task7.di

import com.example.clevertec_task7.data.remote.FormApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import javax.inject.Singleton

@Module
class RetrofitModule {

    @Provides
    @Singleton
    fun provideFormApi(): FormApi {
        return Retrofit.Builder()
            .baseUrl("http://test.clevertec.ru/tt/")
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(FormApi::class.java)
    }
}