package com.example.clevertec_task7.di

import com.example.clevertec_task7.data.repository.Repository
import com.example.clevertec_task7.domain.use_cases.GetFormUseCase
import com.example.clevertec_task7.domain.use_cases.PostFormUseCase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class UseCasesModule {
    @Provides
    @Singleton
    fun provideGetFormUseCase(repository: Repository): GetFormUseCase {
        return GetFormUseCase(repository)
    }

    @Provides
    @Singleton
    fun providePostFormUseCase(repository: Repository): PostFormUseCase {
        return PostFormUseCase(repository)
    }
}