package com.example.clevertec_task7.domain.model

sealed class FormItem {
    data class StringNumberItem(
        val title: String,
        val name: String,
        val type: String
    ) : FormItem()

    data class ListItem(
        val title: String,
        val name: String,
        val type: String,
        val values: Values
    ) : FormItem()
}

