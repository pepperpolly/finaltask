package com.example.clevertec_task7.domain.model

import com.example.clevertec_task7.domain.model.FormItem

data class FullFormItem(var title: String, var image: String, var fields: List<FormItem>)
