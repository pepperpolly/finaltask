package com.example.clevertec_task7.domain.model

data class UserForm(val form: Map<String, String>)
