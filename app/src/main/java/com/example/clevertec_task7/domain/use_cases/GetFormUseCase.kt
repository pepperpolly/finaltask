package com.example.clevertec_task7.domain.use_cases

import com.example.clevertec_task7.domain.model.Resource
import com.example.clevertec_task7.data.repository.Repository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class GetFormUseCase @Inject constructor(private val repository: Repository) {

    operator fun invoke(): Flow<Resource<String>> = flow {
        try {
            val result = repository.getFormInfo()
            emit(Resource.Success(result))
        } catch (e: HttpException) {
            emit(Resource.Error(e.localizedMessage ?: "An unexpected error occured"))
        } catch (e: IOException) {
            emit(
                Resource.Error(
                    e.localizedMessage ?: "Couldn't reach server. Check your internet connection"
                )
            )
        }
    }
}