package com.example.clevertec_task7.domain.use_cases

import com.example.clevertec_task7.domain.model.Resource
import com.example.clevertec_task7.domain.model.UserForm
import com.example.clevertec_task7.data.repository.Repository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class PostFormUseCase @Inject constructor(private val repository: Repository) {

    operator fun invoke(form: UserForm): Flow<Resource<String>> = flow {
        try {
            val result = repository.postForm(form)
            emit(Resource.Success(result))
        } catch (e: HttpException) {
            emit(Resource.Error(e.localizedMessage ?: "An unexpected error occured"))
        } catch (e: IOException) {
            emit(
                Resource.Error(
                    e.localizedMessage ?: "Couldn't reach server. Check your internet connection"
                )
            )
        }
    }
}