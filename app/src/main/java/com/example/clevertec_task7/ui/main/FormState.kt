package com.example.clevertec_task7.ui.main

import com.example.clevertec_task7.domain.model.FullFormItem

data class FormState(val result: FullFormItem? = null, val error: String = "")