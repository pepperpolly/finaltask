package com.example.clevertec_task7.ui.main

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.whenStarted
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.clevertec_task7.R
import com.example.clevertec_task7.common.StateListener
import com.example.clevertec_task7.databinding.MainFragmentBinding
import com.example.clevertec_task7.di.appComponent
import com.example.clevertec_task7.ui.recycler.FormAdapter
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


class MainFragment : Fragment(), StateListener {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var binding: MainFragmentBinding
    private val viewModel: MainViewModel by viewModels {
        requireContext().appComponent.viewModelsFactory()
    }
    private val formAdapter = FormAdapter(this)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.formRecycler.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = formAdapter
        }
        binding.progressBar.visibility = ProgressBar.VISIBLE
        viewModel.getFormInfo()
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.whenStarted {
                viewModel.stateOpen.collect {
                    if (it != null) {
                        if (it.result != null) {
                            formAdapter.submitList(it.result.fields)
                            Glide.with(binding.image.context)
                                .load(it.result.image)
                                .centerCrop()
                                .into(binding.image)
                            binding.toolbar.title = it.result.title
                        } else {
                            Toast.makeText(requireContext(), it.error, Toast.LENGTH_SHORT).show()
                        }
                        binding.progressBar.visibility = ProgressBar.INVISIBLE
                    }
                }
            }
        }
        binding.sendButton.setOnClickListener {
            binding.progressBar.visibility = ProgressBar.VISIBLE
            viewModel.postAnswer()
        }
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.whenStarted {
                viewModel.stateResponseOpen.collect {
                    if (it != null) {
                        if (it.response != "") {
                            showDialog(it.response)
                        } else {
                            Toast.makeText(requireContext(), it.error, Toast.LENGTH_SHORT).show()
                        }
                        binding.progressBar.visibility = ProgressBar.INVISIBLE
                    }
                }
            }
        }
    }

    override fun changeState(tile: String, state: String) {
        viewModel.changeState(tile, state)
    }

    private fun showDialog(result: String) {
        AlertDialog.Builder(requireActivity()).apply {
            setTitle(getString(R.string.result))
            setMessage(result)
            setPositiveButton(getString(R.string.positive_button_text)) { _, _ -> }
            setCancelable(true)
        }.create().show()
    }

}