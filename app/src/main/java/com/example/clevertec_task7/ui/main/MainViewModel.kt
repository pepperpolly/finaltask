package com.example.clevertec_task7.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.clevertec_task7.common.*
import com.example.clevertec_task7.domain.model.Resource
import com.example.clevertec_task7.domain.model.UserForm
import com.example.clevertec_task7.domain.use_cases.GetFormUseCase
import com.example.clevertec_task7.domain.use_cases.PostFormUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val postFormUseCase: PostFormUseCase,
    private val getFormUseCase: GetFormUseCase
) : ViewModel() {

    private val state: MutableStateFlow<FormState?> = MutableStateFlow(null)
    val stateOpen = state as StateFlow<FormState?>

    private val stateResponse: MutableStateFlow<ResponseState?> = MutableStateFlow(null)
    val stateResponseOpen = stateResponse as StateFlow<ResponseState?>

    private val formParser = FormParser()
    private val formState = mutableMapOf<String, String>()

    fun getFormInfo() {
        getFormUseCase.invoke().onEach { result ->
            when (result) {
                is Resource.Success -> {
                    state.value = FormState(result = result.data?.let { formParser.getForm(it) })
                }
                is Resource.Error -> {
                    state.value = FormState(
                        error = result.message ?: "An unexpected error occurred"
                    )
                }
            }
        }.launchIn(viewModelScope)
    }

    fun changeState(title: String, state: String) {
        formState[title] = state
    }

    fun postAnswer() {
        postFormUseCase.invoke(UserForm(formState)).onEach { result ->
            when (result) {
                is Resource.Success -> {
                    stateResponse.value = result.data?.let { formParser.getResponse(it) }?.let {
                        ResponseState(
                            response = it
                        )
                    }
                }
                is Resource.Error -> {
                    stateResponse.value = ResponseState(
                        error = result.message ?: "An unexpected error occurred"
                    )
                }
            }
        }.launchIn(viewModelScope)
    }
}