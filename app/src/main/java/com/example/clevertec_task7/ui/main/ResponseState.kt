package com.example.clevertec_task7.ui.main

data class ResponseState(val response: String = "", val error: String = "")