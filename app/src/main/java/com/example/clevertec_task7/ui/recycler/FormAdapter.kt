package com.example.clevertec_task7.ui.recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.example.clevertec_task7.R
import com.example.clevertec_task7.domain.model.FormItem
import com.example.clevertec_task7.common.StateListener
import com.example.clevertec_task7.databinding.ListItemBinding
import com.example.clevertec_task7.databinding.NumberItemBinding
import com.example.clevertec_task7.databinding.TextItemBinding

class FormAdapter(private val listener: StateListener) :
    ListAdapter<FormItem, FormItemViewHolder>(FormDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FormItemViewHolder {
        return when (viewType) {
            R.layout.text_item -> FormItemViewHolder.TextViewHolder(
                TextItemBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
            R.layout.number_item -> FormItemViewHolder.NumberViewHolder(
                NumberItemBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
            R.layout.list_item -> FormItemViewHolder.ListViewHolder(
                ListItemBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
            else -> throw IllegalArgumentException("Invalid ViewType Provided")
        }
    }

    override fun onBindViewHolder(holder: FormItemViewHolder, position: Int) {
        when (holder) {
            is FormItemViewHolder.TextViewHolder -> holder.bind(
                currentList[position] as FormItem.StringNumberItem,
                listener
            )
            is FormItemViewHolder.NumberViewHolder -> {
                val field = currentList[position] as FormItem.StringNumberItem
                holder.bind(field, listener)
            }
            is FormItemViewHolder.ListViewHolder -> {
                val field = currentList[position] as FormItem.ListItem
                holder.bind(field, listener)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (currentList[position]) {
            is FormItem.StringNumberItem -> if ((currentList[position] as FormItem.StringNumberItem).type == "TEXT") R.layout.text_item else R.layout.number_item
            is FormItem.ListItem -> R.layout.list_item
        }
    }

}