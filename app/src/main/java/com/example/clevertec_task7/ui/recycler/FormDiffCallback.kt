package com.example.clevertec_task7.ui.recycler

import androidx.recyclerview.widget.DiffUtil
import com.example.clevertec_task7.domain.model.FormItem

class FormDiffCallback : DiffUtil.ItemCallback<FormItem>() {
    override fun areItemsTheSame(oldItem: FormItem, newItem: FormItem): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: FormItem, newItem: FormItem): Boolean {
        return oldItem == newItem
    }
}