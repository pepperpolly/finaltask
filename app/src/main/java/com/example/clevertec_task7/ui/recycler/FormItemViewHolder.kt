package com.example.clevertec_task7.ui.recycler

import android.R
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.example.clevertec_task7.domain.model.FormItem
import com.example.clevertec_task7.common.StateListener
import com.example.clevertec_task7.databinding.ListItemBinding
import com.example.clevertec_task7.databinding.NumberItemBinding
import com.example.clevertec_task7.databinding.TextItemBinding


sealed class FormItemViewHolder(
    private val binding: ViewBinding
) :
    RecyclerView.ViewHolder(binding.root) {

    class TextViewHolder(private val binding: TextItemBinding) : FormItemViewHolder(binding) {
        fun bind(field: FormItem.StringNumberItem, listener: StateListener) {
            binding.title.text = field.title
            binding.inputData.addTextChangedListener {
                listener.changeState(
                    field.name,
                    binding.inputData.text.toString()
                )
            }
        }
    }

    class NumberViewHolder(private val binding: NumberItemBinding) : FormItemViewHolder(binding) {
        fun bind(field: FormItem.StringNumberItem, listener: StateListener) {
            binding.title.text = field.title
            binding.inputData.addTextChangedListener {
                listener.changeState(
                    field.name,
                    binding.inputData.text.toString()
                )
            }
        }
    }

    class ListViewHolder(private val binding: ListItemBinding) :
        FormItemViewHolder(binding) {
        fun bind(field: FormItem.ListItem, listener: StateListener) {
            binding.title.text = field.title
            val adapter: ArrayAdapter<String> =
                ArrayAdapter<String>(
                    binding.root.context,
                    R.layout.simple_spinner_item,
                    listOf(field.values.none, field.values.v1, field.values.v2, field.values.v3)
                )
            adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
            binding.inputData.adapter = adapter
            binding.inputData.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    listener.changeState(
                        field.name,
                        binding.inputData.selectedItem.toString()
                    )
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {}
            }
        }
    }

}
